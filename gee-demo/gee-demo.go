package main

import (
	"gitee.com/caijian76/gee"
)

func main() {
	web := gee.Defaule()

	web.GET("/", func(c *gee.Context) {
		c.String(200, "hello")
	})

	web.GET("/api/version", func(c *gee.Context) {
		c.String(200, "version:v1.0")
	})
	web.GET("/panic", func(c *gee.Context) {
		names := []string{"geektutu"}
		c.String(200, names[100])
	})
	api := web.Group("/api")

	api.Static("/static", "./static")
	api.GET("/:name", func(c *gee.Context) {
		c.String(200, ":name")
	})
	api.GET("/ping", func(c *gee.Context) {
		c.String(200, "pong")
	})
	api.POST("/v1", func(c *gee.Context) {
		name := c.Query("name")
		c.JSON(200, gee.H{
			"name":   name,
			"status": "ok",
			"a":      "c",
		})

	})
	api.POST("/v1/*", func(c *gee.Context) {
		name := c.Query("name")
		c.JSON(200, gee.H{
			"name":   name,
			"status": "ok",
			"a":      "b",
		})

	})
	web.Getroute("/api/")
	web.Run(":8080")

}
